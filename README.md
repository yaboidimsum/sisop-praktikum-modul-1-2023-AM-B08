# Praktikum 1 Sistem Operasi 
Perkenalkan kami dari kelas ``Sistem Operasi B Kelompok  B08``, dengan Anggota sebagai berikut:

| Nama                      | NRP        |
|---------------------------|------------|
|Dimas Prihady Setyawan     | 5025211184 |
|Yusuf Hasan Nazila         | 5025211255 |
|Ahda Filza Ghaffaru        | 5025211144 |

# Penjelasan soal nomor 3
Instruksi untuk membuat sistem registrasi dan login. Registrasi akan dibuat dalam script ``louis.sh`` dan setiap pengguna yang berhasil mendaftar akan disimpan ke dalam file ``/users/user.txt``. Sistem login akan dibuat pada script ``retep.sh``.

### Langkah-langkah
- Membuat File ``log.txt`` yang bertujuan untuk mencatat login dan registrasi dengan command sebagai berikut
```
touch log.txt
```
- Membuat folder users untuk menyimpan file ``user.txt`` yang memiliki informasi username dan password

```
mkdir users
touch users/users.txt
```
- Membuat ``louis.sh`` untuk registrasi username dan password yang telah ditentukan yaitu

```
nano louis.sh
```

Setelah berhasil dibuat, username dan password disimpan ke ``users/users.txt`` dan status registrasi terupdate di log.txt


- Meminta username dan password dari user untuk registrasi
Meminta input username dan password dari pengguna
```
read -p "Masukkan username: " username
read -p "Masukkan password: " password
```
- Membuat variabel time untuk dimasukan ke ``log.txt``

```
time=$(date +"%y/%m/%d %H:%M:%S")
```
- Mengecek apakah username sudah pernah dibuat atau tidak, jika iya maka akan mengirim informasi ke ``log.txt`` bahwa user telah ada. Namun, jika tidak ada, maka akan berlanjut bikin password sesuai dengan kriteria yang telah ditentukan 

1. Minimal 8 karakter
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
3. Alphanumeric
4. Tidak boleh sama dengan username 
5. Tidak boleh menggunakan kata chicken atau ernie
```
# Mencari apakah username sudah terdaftar atau belum
if grep -q "^$username:" "users/users.txt"; then
    echo "Username sudah terdaftar."
    echo "$time REGISTER: ERROR User already exists" >> log.txt

else

    # Menentukan kriteria password yang aman
    length=$(echo -n $password | wc -c)
    has_lower=$(echo -n $password | grep -c '[a-z]')
    has_upper=$(echo -n $password | grep -c '[A-Z]')
    has_digit=$(echo -n $password | grep -c '[0-9]')
    is_alphanumeric=$(echo -n $password | grep -c '^[[:alnum:]]*$')
    not_username=$(echo -n $password | grep -c "^$username$")
    contains_chicken=$(echo -n $password | grep -c 'chicken')
    contains_ernie=$(echo -n $password | grep -c 'ernie')

    # Memeriksa apakah password memenuhi kriteria yang ditentukan
    if [ $contains_chicken -eq 1 ] || [ $contains_ernie -eq 1 ]; then
        echo "Password tidak boleh menggunakan kata 'chicken' atau 'ernie'."
    elif [ $not_username -eq 1 ]; then
        echo "Password tidak boleh sama dengan username."
    elif [ $length -lt 8 ]; then
        echo "Password harus minimal 8 karakter."
    elif [ $has_lower -eq 0 ] || [ $has_upper -eq 0 ]; then
        echo "Password harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    elif [ $is_alphanumeric -eq 0 ] || [ $has_digit -eq 0 ]; then
        echo "Password harus alphanumeric"
```
- Setelah password berhasil dibuat, maka username dan password akan dimasukan ke `users/users.txt` dan mengirim informasi ke `log.txt` bahwa Registrasi telah berhasil
```
    else
        # Menambahkan username dan password ke dalam file /users/users.txt
        echo "$username:$password" >> users/users.txt
        echo "$time REGISTER: INFO User $username registered successfully" >> log.txt
        echo "Pendaftaran user berhasil!"
    fi
fi
``` 
Setelah file ``louis.sh`` untuk registrasi dibuat, buatlah file ``retep.sh`` untuk login berdasarkan username dan password yang telah dibuat dari file ``louis.sh`` serta ditulis di file ``users/users.txt``.

```
nano retep.sh
```
- Meminta untuk memasukan username dan passowrd yang telah terdaftar dari file ``louis.sh`` dan disetor di ``users/users.txt``
```
# Meminta input username dan password dari pengguna
read -p "Masukkan username: " username
read -p "Masukkan password: " password
```
- Mencari username dan password yang tersimpan dari file ``users/users.txt``
```
# Mencari username dan password pada file /users/users.txt
result=$(grep "^$username:" users/users.txt)
```
- Menentukan waktu saat pencarian username dan password
```
# Menentukan waktu
time=$(date +"%y/%m/%d %H:%M:%S")
```
- Mencari kecocokan antar username serta password dan untuk tiap kondisi status login dimasukan ke log.txt
```
# Memeriksa apakah username ditemukan dan password cocok
if [ -n "$result" ]; then
    stored_password=$(echo $result | cut -d ':' -f 2 | tr -d ' ')
    if [ "$password" = "$stored_password" ]; then
        echo "Selamat datang, $username!"
        echo "$time LOGIN: INFO User $username logged in" >> log.txt
    else
        echo "Password yang dimasukkan salah."
        echo "$time LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    fi
else
    echo "Login gagal: username $username tidak ditemukan."
    echo "$time LOGIN: ERROR Username $username not found" >> log.txt
fi
```
